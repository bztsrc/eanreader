EAN reader
==========

Basically a very minimalist WEB API frontend for an USB barcode scanner.

A quick and dirty tool that I wrote for the Raspberry Pi. The executable should be run from the `/etc/rc.local` file. It
gets the input from an USB barcode reader on stdin and buffers it, which then can be accessed through HTTP. The request
is not parsed, there's only one kind of response in plain text format (each barcode in a separate line, ended in '\r\n').
Whenever the WEB API is accessed, the buffer is cleared. If there were no barcode reads between two adjacent WEB API calls,
then it simply returns HTTP 200 OK with an empty body.

On start, it clears the screen and displays the WEB API URL it provides. Exit with <kbd>Ctrl</kbd> + <kbd>C</kbd>.
