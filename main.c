/*
 * Copyright (C) 2020 bzt (bztsrc@github)
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * @brief An extremely simple and minimalist stdin to WEB API proxy
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define PORT 8080           /* http port to use */
#define MAXBUFF 65536       /* maximum buffer size to store barcodes */

/**
 * Clear the screen and show banner
 */
void clrscreen(char *ip)
{
    printf("\033[H\033[2J\n  http://%s:%d/\n\n", ip, PORT);
}

/**
 * Main procedure
 */
int main(int argc, char **argv)
{
    int sock, msgsock, val = 1, ret, outlen = 0;
    socklen_t length = sizeof(struct sockaddr_in);
    struct sockaddr_in server;
    struct ifreq ifr;
    char *c, *ip = NULL, buf[1024], out[MAXBUFF];
    fd_set rdfs;

    memset(out, 0, sizeof(out));
    memset(&server, 0, length);

    /* open TCP port */
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(PORT);
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if(sock < 0) { perror("opening stream socket"); exit(1); }
    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));
    if( bind(sock, (struct sockaddr *)&server, length) < 0 ||
        getsockname(sock, (struct sockaddr *)&server, &length) < 0 ||
        listen(sock, 16) < 0) { perror("listening on port"); exit(2); }

    /* get my IP address */
    ifr.ifr_addr.sa_family = AF_INET;
    strncpy(ifr.ifr_name, "eth0", IFNAMSIZ-1);
    ioctl(sock, SIOCGIFADDR, &ifr);
    ip = inet_ntoa(((struct sockaddr_in*)&ifr.ifr_addr)->sin_addr);
    if(!ip || !strcmp(ip, "0.0.0.0")) {
        strncpy(ifr.ifr_name, "wlan0", IFNAMSIZ-1);
        ioctl(sock, SIOCGIFADDR, &ifr);
        ip = inet_ntoa(((struct sockaddr_in*)&ifr.ifr_addr)->sin_addr);
    }
    if(!ip) ip = "0.0.0.0";
    clrscreen(ip);

    /* handle connections */
    while(1) {
        FD_ZERO(&rdfs);
        FD_SET(STDIN_FILENO, &rdfs);
        FD_SET(sock, &rdfs);
        select(sock+1, &rdfs, NULL, NULL, NULL);

        /* input from barcode reader, store it in buffer */
        if(FD_ISSET(STDIN_FILENO, &rdfs)) {
            buf[0] = 0;
            fgets(buf, sizeof(buf)-1, stdin);
            clrscreen(ip);
            if(!buf[0]) continue;
            for(c = buf; *c && *c != '\r' && *c != '\n' && outlen < sizeof(out) - 3; c++)
                switch(*c) {
                    case '`': out[outlen++] = '0'; break;
                    case '/': out[outlen++] = '-'; break;
                    default: out[outlen++] = *c; break;
                }
            out[outlen++] = '\r'; out[outlen++] = '\n'; out[outlen] = 0;
        }

        /* WEB API request, send and clear buffer */
        if(FD_ISSET(sock, &rdfs)) {
            msgsock = accept(sock, (struct sockaddr *)0, (socklen_t *)0);
            if(msgsock < 0) continue;
            /* read http request header, no need to parse it */
            do { val = read(msgsock, buf, sizeof(buf)); } while(val == sizeof(buf));
            /* send response */
            dprintf(msgsock,
                "HTTP/1.1 200 OK\r\n"
                "Cache-Control: no-cache, no-store, must-revalidate\r\n"
                "Content-Type: text/plain\r\n"
                "Content-Length: %d\r\n"
                "Connection: close\r\n"
                "\r\n", outlen);
            write(msgsock, out, outlen);
            close(msgsock);
            memset(out, 0, sizeof(out));
            outlen = 0;
        }
    }
}
